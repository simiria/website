[[!meta title="Treasurer's report for May 2022"]]
[[!toc levels=4]]

# Treasurer's report 2022-05-01 - 2022-06-01

This is a *draft*.

## Software in Public Interest Inc

### This month changes 2022-05-01 - 2022-06-01

<pre>
         debit         credit          total
--------------------------------------------
 152278.18 USD  171792.92 USD  -19514.74 USD  Assets
      0.11 USD              0       0.11 USD    Ameriprise Cash Mgmt Acct
      5.00 USD    3769.02 USD   -3764.02 USD    Bank of America Business Advantage Checking
      0.02 USD    5346.08 USD   -5346.06 USD    Chase Business Select High Yield Savings
  17144.55 USD      40.17 USD   17104.38 USD    Chase Performance Business Checking
   2203.72 USD    4494.24 USD   -2290.52 USD    Fifth Third Business Elite Checking (Debian)
   3844.29 USD   21691.00 USD  -17846.71 USD    Fifth Third Business Elite Checking (SPI)
  27843.24 USD   47108.72 USD  -19265.48 USD    Fifth Third Business Elite Checking Wiretransfer
      5.25 USD              0       5.25 USD    Fifth Third Business Money Market 128
     15.14 USD              0      15.14 USD    Key Business Gold Money Market Savings
  26430.89 USD    3175.33 USD   23255.56 USD    Key Business Reward Checking
    378.67 USD              0     378.67 USD    Merrill Lynch EMA
   2190.79 USD    2372.26 USD    -181.47 USD    PaypalDebian
   4093.64 USD    4103.41 USD      -9.77 USD    PaypalSPI
  30543.52 USD   42113.34 USD  -11569.82 USD    Receivable
  37579.35 USD   37579.35 USD              0    Transfers
  42650.48 USD      39.14 USD   42611.34 USD  Expenses
    777.07 USD      19.10 USD     757.97 USD    Bank-Fees
    507.71 USD              0     507.71 USD    Conferences
    430.48 USD              0     430.48 USD      Dining
     77.23 USD              0      77.23 USD      Venue
      3.94 USD              0       3.94 USD    Currency-Exchange
   9094.65 USD              0    9094.65 USD    IT
    308.00 USD              0     308.00 USD      Domains
    830.26 USD              0     830.26 USD      Hardware
   4356.39 USD              0    4356.39 USD      Hosting
   3600.00 USD              0    3600.00 USD      Services
   9345.12 USD              0    9345.12 USD    Legal
     84.74 USD              0      84.74 USD    Office:Postal
  19249.42 USD              0   19249.42 USD    Software-Development
   3587.83 USD      20.04 USD    3567.79 USD    Travel
    378.61 USD              0     378.61 USD      Accommodation
   2168.40 USD      20.04 USD    2148.36 USD      Dining
   1040.82 USD              0    1040.82 USD      Transportation
   1734.90 USD   37270.42 USD  -35535.52 USD  Income
             0     395.14 USD    -395.14 USD    Currency-Exchange
   1719.80 USD   35816.01 USD  -34096.21 USD    Donations
             0      16.59 USD     -16.59 USD    Interest
             0     650.00 USD    -650.00 USD    Sponsorship
     15.10 USD     392.68 USD    -377.58 USD    Unrealized-Gain
  65607.26 USD   53168.34 USD   12438.92 USD  Liabilities:Payable
--------------------------------------------
 262270.82 USD  262270.82 USD              0
</pre>

### Year to date, ending 2022-06-01

<pre>
         debit         credit          total
--------------------------------------------
4005870.11 USD  900476.19 USD 3105393.92 USD  Assets
             0   80625.21 USD  -80625.21 USD    Accumulated-Depreciation
  13916.73 USD              0   13916.73 USD    Ameriprise Cash Mgmt Acct
  84350.51 USD   10545.21 USD   73805.30 USD    Bank of America Business Advantage Checking
   5346.08 USD    5346.08 USD              0    Chase Business Select High Yield Savings
 262635.11 USD   21968.81 USD  240666.30 USD    Chase Performance Business Checking
  24865.44 USD   23865.44 USD    1000.00 USD    Fifth Third Business Elite Checking (Debian)
  61514.17 USD   60513.97 USD    1000.20 USD    Fifth Third Business Elite Checking (SPI)
 211386.09 USD  153494.74 USD   57891.35 USD    Fifth Third Business Elite Checking Wiretransfer
  29919.02 USD              0   29919.02 USD    Fifth Third Business Money Market 128
 206837.73 USD              0  206837.73 USD    Fixed-Assets
1781822.83 USD              0 1781822.83 USD    Key Business Gold Money Market Savings
 461415.04 USD   12864.25 USD  448550.79 USD    Key Business Reward Checking
   1000.00 USD              0    1000.00 USD    KeyBank Basic Business Checking
 312541.42 USD   31655.82 USD  280885.60 USD    Merrill Lynch EMA
  26119.57 USD   24192.01 USD    1927.56 USD    PaypalDebian
  65994.50 USD   62263.33 USD    3731.17 USD    PaypalSPI
     27.80 USD      27.80 USD              0    Prepaid-Expenses
 227793.88 USD  184729.33 USD   43064.55 USD    Receivable
 228384.19 USD  228384.19 USD              0    Transfers
             0 3073194.99 USD -3073194.99 USD  Equity:Net-Assets
             0 2572016.86 USD -2572016.86 USD    Restricted
             0  501178.13 USD -501178.13 USD    Unrestricted
 173463.38 USD    1362.47 USD  172100.91 USD  Expenses
  16452.43 USD              0   16452.43 USD    Accounting
   4248.75 USD      36.14 USD    4212.61 USD    Bank-Fees
    530.86 USD              0     530.86 USD    Conferences
    430.48 USD              0     430.48 USD      Dining
     23.15 USD              0      23.15 USD      Materials
     77.23 USD              0      77.23 USD      Venue
     90.22 USD              0      90.22 USD    Currency-Exchange
     39.84 USD      39.84 USD              0    Disassociation
  34564.59 USD       0.22 USD   34564.37 USD    IT
    677.57 USD              0     677.57 USD      Domains
   1720.48 USD              0    1720.48 USD      Hardware
  21166.54 USD       0.22 USD   21166.32 USD      Hosting
  11000.00 USD              0   11000.00 USD      Services
  47964.46 USD              0   47964.46 USD    Legal
    777.28 USD              0     777.28 USD    Office
    697.43 USD              0     697.43 USD      Postal
     79.85 USD              0      79.85 USD      Supplies
  32285.07 USD              0   32285.07 USD    Software-Development
   3587.83 USD      20.04 USD    3567.79 USD    Travel
    378.61 USD              0     378.61 USD      Accommodation
   2168.40 USD      20.04 USD    2148.36 USD      Dining
   1040.82 USD              0    1040.82 USD      Transportation
  32922.05 USD    1266.23 USD   31655.82 USD    Unrealized-Loss
   8364.33 USD  198068.17 USD -189703.84 USD  Income
             0    1602.80 USD   -1602.80 USD    Currency-Exchange
   8056.71 USD  187311.15 USD -179254.44 USD    Donations
     48.76 USD    1348.72 USD   -1299.96 USD    Interest
             0    1075.00 USD   -1075.00 USD    Sponsorship
    258.86 USD    6730.50 USD   -6471.64 USD    Unrealized-Gain
 231845.23 USD  246441.23 USD  -14596.00 USD  Liabilities:Payable
--------------------------------------------
4419543.05 USD 4419543.05 USD              0
</pre>

## Per project summaries

Per project donations have a debit amount specified, which is the SPI
5% contribution from the project towards the SPI general fund. Thus
total donation amount is net of this contribution.

Unlike ledger defaults, totals in the per-project summaries are
inverted. Meaning that expenses are negative and equity is
positive. This way projects that have funds, i.e. are in credit, are
shown as a positive amount.

### This month changes 2022-05-01 - 2022-06-01


#### 0ad

<pre>
         debit         credit          total
--------------------------------------------
     80.75 USD      19.10 USD     -61.65 USD  Expenses
     29.84 USD      19.10 USD     -10.74 USD    Bank-Fees
     50.91 USD              0     -50.91 USD    IT:Hosting
     25.35 USD     108.20 USD      82.85 USD  Income:Donations
--------------------------------------------
    106.10 USD     127.30 USD      21.20 USD

</pre>

#### adelielinux

<pre>
         debit         credit          total
--------------------------------------------
      5.70 USD              0      -5.70 USD  Expenses:Bank-Fees
     13.09 USD     261.70 USD     248.61 USD  Income:Donations
--------------------------------------------
     18.79 USD     261.70 USD     242.91 USD

</pre>

#### archlinux

<pre>
         debit         credit          total
--------------------------------------------
    627.33 USD              0    -627.33 USD  Expenses
    118.37 USD              0    -118.37 USD    Bank-Fees
    508.96 USD              0    -508.96 USD    IT
    171.69 USD              0    -171.69 USD      Hardware
    337.27 USD              0    -337.27 USD      Hosting
    118.67 USD    2374.55 USD    2255.88 USD  Income
             0       1.45 USD       1.45 USD    Currency-Exchange
    118.67 USD    2373.10 USD    2254.43 USD    Donations
--------------------------------------------
    746.00 USD    2374.55 USD    1628.55 USD

</pre>

#### ardupilot

<pre>
         debit         credit          total
--------------------------------------------
   2604.26 USD              0   -2604.26 USD  Expenses
    162.03 USD              0    -162.03 USD    Bank-Fees
      3.50 USD              0      -3.50 USD    Currency-Exchange
    738.73 USD              0    -738.73 USD    IT:Hosting
   1700.00 USD              0   -1700.00 USD    Software-Development
    152.00 USD    3304.89 USD    3152.89 USD  Income
             0     264.89 USD     264.89 USD    Currency-Exchange
    152.00 USD    2740.00 USD    2588.00 USD    Donations
             0     300.00 USD     300.00 USD    Sponsorship
--------------------------------------------
   2756.26 USD    3304.89 USD     548.63 USD

</pre>

#### debconf22

<pre>
         debit         credit          total
--------------------------------------------
      1.14 USD              0      -1.14 USD  Expenses:Bank-Fees
    578.50 USD   11570.00 USD   10991.50 USD  Income
    578.50 USD   11220.00 USD   10641.50 USD    Donations
             0     350.00 USD     350.00 USD    Sponsorship
--------------------------------------------
    579.64 USD   11570.00 USD   10990.36 USD

</pre>

#### debian

<pre>
         debit         credit          total
--------------------------------------------
  12456.92 USD              0  -12456.92 USD  Expenses
    219.18 USD              0    -219.18 USD    Bank-Fees
    507.71 USD              0    -507.71 USD    Conferences
    430.48 USD              0    -430.48 USD      Dining
     77.23 USD              0     -77.23 USD      Venue
      0.44 USD              0      -0.44 USD    Currency-Exchange
    966.57 USD              0    -966.57 USD    IT
    308.00 USD              0    -308.00 USD      Domains
    658.57 USD              0    -658.57 USD      Hardware
   9345.12 USD              0   -9345.12 USD    Legal
   1417.90 USD              0   -1417.90 USD    Travel
    378.61 USD              0    -378.61 USD      Accommodation
     69.26 USD              0     -69.26 USD      Dining
    970.03 USD              0    -970.03 USD      Transportation
    145.53 USD    2944.35 USD    2798.82 USD  Income
             0      34.50 USD      34.50 USD    Currency-Exchange
    130.43 USD    2607.79 USD    2477.36 USD    Donations
     15.10 USD     302.06 USD     286.96 USD    Unrealized-Gain
--------------------------------------------
  12602.45 USD    2944.35 USD   -9658.10 USD

</pre>

#### ffmpeg

<pre>
         debit         credit          total
--------------------------------------------
     18.62 USD              0     -18.62 USD  Expenses:Bank-Fees
     16.11 USD     322.25 USD     306.14 USD  Income:Donations
--------------------------------------------
     34.73 USD     322.25 USD     287.52 USD

</pre>

#### haskell

<pre>
         debit         credit          total
--------------------------------------------
  17549.42 USD              0  -17549.42 USD  Expenses:Software-Development

</pre>

#### libreoffice

<pre>
         debit         credit          total
--------------------------------------------
      4.07 USD              0      -4.07 USD  Expenses:Bank-Fees
     35.55 USD     711.00 USD     675.45 USD  Income:Donations
--------------------------------------------
     39.62 USD     711.00 USD     671.38 USD

</pre>

#### ntpsec

<pre>
         debit         credit          total
--------------------------------------------
      0.52 USD              0      -0.52 USD  Expenses:Bank-Fees
      0.05 USD       1.00 USD       0.95 USD  Income:Donations
--------------------------------------------
      0.57 USD       1.00 USD       0.43 USD

</pre>

#### openbioinformatics

<pre>
         debit         credit          total
--------------------------------------------
     40.15 USD              0     -40.15 USD  Expenses:IT:Hosting
     50.00 USD    1000.00 USD     950.00 USD  Income:Donations
--------------------------------------------
     90.15 USD    1000.00 USD     909.85 USD

</pre>

#### openembedded

<pre>
         debit         credit          total
--------------------------------------------
      1.38 USD              0      -1.38 USD  Expenses:Bank-Fees
      1.00 USD      20.00 USD      19.00 USD  Income:Donations
--------------------------------------------
      2.38 USD      20.00 USD      17.62 USD

</pre>

#### openwrt

<pre>
         debit         credit          total
--------------------------------------------
      1.38 USD              0      -1.38 USD  Expenses:Bank-Fees
      1.00 USD      20.00 USD      19.00 USD  Income:Donations
--------------------------------------------
      2.38 USD      20.00 USD      17.62 USD

</pre>

#### openzfs

<pre>
         debit         credit          total
--------------------------------------------
    123.10 USD              0    -123.10 USD  Expenses
      6.61 USD              0      -6.61 USD    Bank-Fees
    116.49 USD              0    -116.49 USD    IT:Hosting
      1.25 USD      25.00 USD      23.75 USD  Income:Donations
--------------------------------------------
    124.35 USD      25.00 USD     -99.35 USD

</pre>

#### postgresql

<pre>
         debit         credit          total
--------------------------------------------
     67.39 USD              0     -67.39 USD  Expenses:Bank-Fees
     89.85 USD    1797.00 USD    1707.15 USD  Income:Donations
--------------------------------------------
    157.24 USD    1797.00 USD    1639.76 USD

</pre>

#### spi

<pre>
         debit         credit          total
--------------------------------------------
   6046.53 USD      20.04 USD   -6026.49 USD  Expenses
    115.52 USD              0    -115.52 USD    Bank-Fees
   3676.34 USD              0   -3676.34 USD    IT
     76.34 USD              0     -76.34 USD      Hosting
   3600.00 USD              0   -3600.00 USD      Services
     84.74 USD              0     -84.74 USD    Office:Postal
   2169.93 USD      20.04 USD   -2149.89 USD    Travel
   2099.14 USD      20.04 USD   -2079.10 USD      Dining
     70.79 USD              0     -70.79 USD      Transportation
      1.00 USD    2691.48 USD    2690.48 USD  Income
             0      94.30 USD      94.30 USD    Currency-Exchange
      1.00 USD    2489.97 USD    2488.97 USD    Donations
             0      16.59 USD      16.59 USD    Interest
             0      90.62 USD      90.62 USD    Unrealized-Gain
--------------------------------------------
   6047.53 USD    2711.52 USD   -3336.01 USD

</pre>

#### texmacs

<pre>
         debit         credit          total
--------------------------------------------
      1.54 USD              0      -1.54 USD  Expenses:Bank-Fees
      1.50 USD      30.00 USD      28.50 USD  Income:Donations
--------------------------------------------
      3.04 USD      30.00 USD      26.96 USD

</pre>

#### translatewiki.net

<pre>
         debit         credit          total
--------------------------------------------
      0.55 USD              0      -0.55 USD  Expenses:Bank-Fees
      0.15 USD       3.00 USD       2.85 USD  Income:Donations
--------------------------------------------
      0.70 USD       3.00 USD       2.30 USD

</pre>

#### xorg

<pre>
         debit         credit          total
--------------------------------------------
   3019.73 USD              0   -3019.73 USD  Expenses
     23.23 USD              0     -23.23 USD    Bank-Fees
   2996.50 USD              0   -2996.50 USD    IT:Hosting
    504.30 USD   10086.00 USD    9581.70 USD  Income:Donations
--------------------------------------------
   3524.03 USD   10086.00 USD    6561.97 USD
</pre>

### Year to date, ending 2022-06-01


#### 0ad

<pre>
         debit         credit          total
--------------------------------------------
             0   41992.95 USD   41992.95 USD  Equity:Net-Assets:Restricted
    369.01 USD      19.10 USD    -349.91 USD  Expenses
    100.50 USD      19.10 USD     -81.40 USD    Bank-Fees
    268.51 USD              0    -268.51 USD    IT:Hosting
     72.92 USD    1059.42 USD     986.50 USD  Income:Donations
--------------------------------------------
    441.93 USD   43071.47 USD   42629.54 USD

</pre>

#### adelielinux

<pre>
         debit         credit          total
--------------------------------------------
             0      32.15 USD      32.15 USD  Equity:Net-Assets:Restricted
    127.60 USD              0    -127.60 USD  Expenses:Bank-Fees
     76.50 USD    6214.20 USD    6137.70 USD  Income:Donations
--------------------------------------------
    204.10 USD    6246.35 USD    6042.25 USD

</pre>

#### ankur

<pre>
         debit         credit          total
--------------------------------------------
             0    2819.84 USD    2819.84 USD  Equity:Net-Assets:Restricted

</pre>

#### aptosid

<pre>
         debit         credit          total
--------------------------------------------
             0     503.76 USD     503.76 USD  Equity:Net-Assets:Restricted

</pre>

#### archlinux

<pre>
         debit         credit          total
--------------------------------------------
             0  392203.28 USD  392203.28 USD  Equity:Net-Assets:Restricted
   2989.72 USD              0   -2989.72 USD  Expenses
    786.09 USD              0    -786.09 USD    Bank-Fees
   2203.63 USD              0   -2203.63 USD    IT
    171.69 USD              0    -171.69 USD      Hardware
   2031.94 USD              0   -2031.94 USD      Hosting
    862.46 USD   17247.97 USD   16385.51 USD  Income
             0       1.45 USD       1.45 USD    Currency-Exchange
    862.46 USD   17246.52 USD   16384.06 USD    Donations
--------------------------------------------
   3852.18 USD  409451.25 USD  405599.07 USD

</pre>

#### archlinux32

<pre>
         debit         credit          total
--------------------------------------------
             0      17.96 USD      17.96 USD  Equity:Net-Assets:Restricted

</pre>

#### ardupilot

<pre>
         debit         credit          total
--------------------------------------------
             0  111924.67 USD  111924.67 USD  Equity:Net-Assets:Restricted
  17904.26 USD              0  -17904.26 USD  Expenses
    317.51 USD              0    -317.51 USD    Bank-Fees
      3.50 USD              0      -3.50 USD    Currency-Exchange
   2983.25 USD              0   -2983.25 USD    IT:Hosting
  14600.00 USD              0  -14600.00 USD    Software-Development
    287.50 USD    6155.60 USD    5868.10 USD  Income
             0     405.60 USD     405.60 USD    Currency-Exchange
    287.50 USD    5200.00 USD    4912.50 USD    Donations
             0     550.00 USD     550.00 USD    Sponsorship
--------------------------------------------
  18191.76 USD  118080.27 USD   99888.51 USD

</pre>

#### chakra

<pre>
         debit         credit          total
--------------------------------------------
      0.22 USD       0.22 USD              0  Expenses:IT:Hosting

</pre>

#### debconf20

<pre>
         debit         credit          total
--------------------------------------------
             0    1634.54 USD    1634.54 USD  Equity:Net-Assets:Restricted

</pre>

#### debconf21

<pre>
         debit         credit          total
--------------------------------------------
             0    8321.00 USD    8321.00 USD  Equity:Net-Assets:Restricted
     23.15 USD              0     -23.15 USD  Expenses:Conferences:Materials
--------------------------------------------
     23.15 USD    8321.00 USD    8297.85 USD

</pre>

#### debconf22

<pre>
         debit         credit          total
--------------------------------------------
             0   24685.00 USD   24685.00 USD  Equity:Net-Assets:Restricted
      1.89 USD              0      -1.89 USD  Expenses:Bank-Fees
    849.00 USD   16980.00 USD   16131.00 USD  Income
    849.00 USD   16455.00 USD   15606.00 USD    Donations
             0     525.00 USD     525.00 USD    Sponsorship
--------------------------------------------
    850.89 USD   41665.00 USD   40814.11 USD

</pre>

#### debconf23

<pre>
         debit         credit          total
--------------------------------------------
             0    4750.00 USD    4750.00 USD  Equity:Net-Assets:Restricted

</pre>

#### debian

<pre>
         debit         credit          total
--------------------------------------------
             0  780042.00 USD  780042.00 USD  Equity:Net-Assets:Restricted
  77828.46 USD    1283.21 USD  -76545.25 USD  Expenses
   1364.59 USD      16.98 USD   -1347.61 USD    Bank-Fees
    507.71 USD              0    -507.71 USD    Conferences
    430.48 USD              0    -430.48 USD      Dining
     77.23 USD              0     -77.23 USD      Venue
      0.44 USD              0      -0.44 USD    Currency-Exchange
   1888.71 USD              0   -1888.71 USD    IT
    339.92 USD              0    -339.92 USD      Domains
   1548.79 USD              0   -1548.79 USD      Hardware
  47324.46 USD              0  -47324.46 USD    Legal
   1417.90 USD              0   -1417.90 USD    Travel
    378.61 USD              0    -378.61 USD      Accommodation
     69.26 USD              0     -69.26 USD      Dining
    970.03 USD              0    -970.03 USD      Transportation
  25324.65 USD    1266.23 USD  -24058.42 USD    Unrealized-Loss
   1184.51 USD   23705.34 USD   22520.83 USD  Income
             0     402.47 USD     402.47 USD    Currency-Exchange
    876.89 USD   17150.37 USD   16273.48 USD    Donations
     48.76 USD     975.19 USD     926.43 USD    Interest
    258.86 USD    5177.31 USD    4918.45 USD    Unrealized-Gain
--------------------------------------------
  79012.97 USD  805030.55 USD  726017.58 USD

</pre>

#### ffmpeg

<pre>
         debit         credit          total
--------------------------------------------
             0  139579.15 USD  139579.15 USD  Equity:Net-Assets:Restricted
    122.16 USD              0    -122.16 USD  Expenses:Bank-Fees
    353.84 USD    7076.92 USD    6723.08 USD  Income:Donations
--------------------------------------------
    476.00 USD  146656.07 USD  146180.07 USD

</pre>

#### fluxbox

<pre>
         debit         credit          total
--------------------------------------------
             0    1046.33 USD    1046.33 USD  Equity:Net-Assets:Restricted

</pre>

#### gallery

<pre>
         debit         credit          total
--------------------------------------------
             0      95.00 USD      95.00 USD  Equity:Net-Assets:Restricted

</pre>

#### ganeti

<pre>
         debit         credit          total
--------------------------------------------
             0     183.61 USD     183.61 USD  Equity:Net-Assets:Restricted

</pre>

#### glucosio

<pre>
         debit         credit          total
--------------------------------------------
             0      39.84 USD      39.84 USD  Equity:Net-Assets:Restricted
     39.84 USD              0     -39.84 USD  Expenses:Disassociation
--------------------------------------------
     39.84 USD      39.84 USD              0

</pre>

#### gnustep

<pre>
         debit         credit          total
--------------------------------------------
             0     180.50 USD     180.50 USD  Equity:Net-Assets:Restricted

</pre>

#### haskell

<pre>
         debit         credit          total
--------------------------------------------
             0   17549.42 USD   17549.42 USD  Equity:Net-Assets:Restricted
  17549.42 USD              0  -17549.42 USD  Expenses:Software-Development
--------------------------------------------
  17549.42 USD   17549.42 USD              0

</pre>

#### jenkins

<pre>
         debit         credit          total
--------------------------------------------
             0    9380.96 USD    9380.96 USD  Equity:Net-Assets:Restricted
      3.87 USD              0      -3.87 USD  Expenses:Bank-Fees
      5.00 USD     100.00 USD      95.00 USD  Income:Donations
--------------------------------------------
      8.87 USD    9480.96 USD    9472.09 USD

</pre>

#### libreoffice

<pre>
         debit         credit          total
--------------------------------------------
             0  113720.71 USD  113720.71 USD  Equity:Net-Assets:Restricted
     58.47 USD              0     -58.47 USD  Expenses:Bank-Fees
    133.13 USD    2662.50 USD    2529.37 USD  Income:Donations
--------------------------------------------
    191.60 USD  116383.21 USD  116191.61 USD

</pre>

#### mingw

<pre>
         debit         credit          total
--------------------------------------------
             0    4804.06 USD    4804.06 USD  Equity:Net-Assets:Restricted
      0.69 USD              0      -0.69 USD  Expenses:Bank-Fees
      0.50 USD      10.00 USD       9.50 USD  Income:Donations
--------------------------------------------
      1.19 USD    4814.06 USD    4812.87 USD

</pre>

#### ns-3

<pre>
         debit         credit          total
--------------------------------------------
             0    1140.00 USD    1140.00 USD  Equity:Net-Assets:Restricted

</pre>

#### ntpsec

<pre>
         debit         credit          total
--------------------------------------------
             0  134507.13 USD  134507.13 USD  Equity:Net-Assets:Restricted
      3.21 USD              0      -3.21 USD  Expenses:Bank-Fees
      3.05 USD      61.00 USD      57.95 USD  Income:Donations
--------------------------------------------
      6.26 USD  134568.13 USD  134561.87 USD

</pre>

#### oftc

<pre>
         debit         credit          total
--------------------------------------------
             0      38.00 USD      38.00 USD  Equity:Net-Assets:Restricted

</pre>

#### openbioinformatics

<pre>
         debit         credit          total
--------------------------------------------
             0  125817.84 USD  125817.84 USD  Equity:Net-Assets:Restricted
    469.17 USD              0    -469.17 USD  Expenses
    201.13 USD              0    -201.13 USD    Bank-Fees
    268.04 USD              0    -268.04 USD    IT
     85.83 USD              0     -85.83 USD      Domains
    182.21 USD              0    -182.21 USD      Hosting
    825.25 USD   16505.00 USD   15679.75 USD  Income:Donations
--------------------------------------------
   1294.42 USD  142322.84 USD  141028.42 USD

</pre>

#### openembedded

<pre>
         debit         credit          total
--------------------------------------------
             0   20848.00 USD   20848.00 USD  Equity:Net-Assets:Restricted
      6.90 USD              0      -6.90 USD  Expenses:Bank-Fees
      5.00 USD     100.00 USD      95.00 USD  Income:Donations
--------------------------------------------
     11.90 USD   20948.00 USD   20936.10 USD

</pre>

#### openmpi

<pre>
         debit         credit          total
--------------------------------------------
             0     658.92 USD     658.92 USD  Equity:Net-Assets:Restricted

</pre>

#### opensaf

<pre>
         debit         credit          total
--------------------------------------------
             0    2445.86 USD    2445.86 USD  Equity:Net-Assets:Restricted

</pre>

#### openvas

<pre>
         debit         credit          total
--------------------------------------------
             0     113.29 USD     113.29 USD  Equity:Net-Assets:Restricted
      0.66 USD              0      -0.66 USD  Expenses:Bank-Fees
      0.25 USD       5.00 USD       4.75 USD  Income:Donations
--------------------------------------------
      0.91 USD     118.29 USD     117.38 USD

</pre>

#### openvoting

<pre>
         debit         credit          total
--------------------------------------------
             0    1785.41 USD    1785.41 USD  Equity:Net-Assets:Restricted
      0.39 USD              0      -0.39 USD  Expenses:Bank-Fees
      9.50 USD     190.00 USD     180.50 USD  Income:Donations
--------------------------------------------
      9.89 USD    1975.41 USD    1965.52 USD

</pre>

#### openwrt

<pre>
         debit         credit          total
--------------------------------------------
             0   13720.56 USD   13720.56 USD  Equity:Net-Assets:Restricted
      8.46 USD              0      -8.46 USD  Expenses:Bank-Fees
      5.15 USD     103.00 USD      97.85 USD  Income:Donations
--------------------------------------------
     13.61 USD   13823.56 USD   13809.95 USD

</pre>

#### openzfs

<pre>
         debit         credit          total
--------------------------------------------
             0   40719.05 USD   40719.05 USD  Equity:Net-Assets:Restricted
   1312.65 USD              0   -1312.65 USD  Expenses
     37.92 USD              0     -37.92 USD    Bank-Fees
   1274.73 USD              0   -1274.73 USD    IT
    251.82 USD              0    -251.82 USD      Domains
   1022.91 USD              0   -1022.91 USD      Hosting
    733.85 USD   14677.00 USD   13943.15 USD  Income:Donations
--------------------------------------------
   2046.50 USD   55396.05 USD   53349.55 USD

</pre>

#### pmix

<pre>
         debit         credit          total
--------------------------------------------
             0      28.45 USD      28.45 USD  Equity:Net-Assets:Restricted

</pre>

#### postgresql

<pre>
         debit         credit          total
--------------------------------------------
             0  197013.34 USD  197013.34 USD  Equity:Net-Assets:Restricted
    364.55 USD              0    -364.55 USD  Expenses
    228.90 USD              0    -228.90 USD    Bank-Fees
    135.65 USD              0    -135.65 USD    Software-Development
    291.35 USD    5826.93 USD    5535.58 USD  Income:Donations
--------------------------------------------
    655.90 USD  202840.27 USD  202184.37 USD

</pre>

#### privoxy

<pre>
         debit         credit          total
--------------------------------------------
             0    7261.70 USD    7261.70 USD  Equity:Net-Assets:Restricted
   3319.95 USD              0   -3319.95 USD  Expenses
     72.93 USD              0     -72.93 USD    Bank-Fees
     27.80 USD              0     -27.80 USD    Currency-Exchange
   3219.22 USD              0   -3219.22 USD    IT:Hosting
     82.86 USD    1657.11 USD    1574.25 USD  Income:Donations
--------------------------------------------
   3402.81 USD    8918.81 USD    5516.00 USD

</pre>

#### smc

<pre>
         debit         credit          total
--------------------------------------------
             0    5975.58 USD    5975.58 USD  Equity:Net-Assets:Restricted

</pre>

#### spi

<pre>
         debit         credit          total
--------------------------------------------
             0  501178.13 USD  501178.13 USD  Equity:Net-Assets:Unrestricted
  43501.20 USD      59.94 USD  -43441.26 USD  Expenses
  16452.43 USD              0  -16452.43 USD    Accounting
    647.88 USD       0.06 USD    -647.82 USD    Bank-Fees
     58.48 USD              0     -58.48 USD    Currency-Exchange
             0      39.84 USD      39.84 USD    Disassociation
  15157.80 USD              0  -15157.80 USD    IT
   4157.80 USD              0   -4157.80 USD      Hosting
  11000.00 USD              0  -11000.00 USD      Services
    640.00 USD              0    -640.00 USD    Legal
    777.28 USD              0    -777.28 USD    Office
    697.43 USD              0    -697.43 USD      Postal
     79.85 USD              0     -79.85 USD      Supplies
   2169.93 USD      20.04 USD   -2149.89 USD    Travel
   2099.14 USD      20.04 USD   -2079.10 USD      Dining
     70.79 USD              0     -70.79 USD      Transportation
   7597.40 USD              0   -7597.40 USD    Unrealized-Loss
      2.16 USD   26120.15 USD   26117.99 USD  Income
             0     793.28 USD     793.28 USD    Currency-Exchange
      2.16 USD   23400.15 USD   23397.99 USD    Donations
             0     373.53 USD     373.53 USD    Interest
             0    1553.19 USD    1553.19 USD    Unrealized-Gain
--------------------------------------------
  43503.36 USD  527358.22 USD  483854.86 USD

</pre>

#### systemd

<pre>
         debit         credit          total
--------------------------------------------
             0  173269.08 USD  173269.08 USD  Equity:Net-Assets:Restricted
      7.15 USD              0      -7.15 USD  Expenses:Bank-Fees
      6.05 USD     121.00 USD     114.95 USD  Income:Donations
--------------------------------------------
     13.20 USD  173390.08 USD  173376.88 USD

</pre>

#### texmacs

<pre>
         debit         credit          total
--------------------------------------------
             0    2525.67 USD    2525.67 USD  Equity:Net-Assets:Restricted
      7.70 USD              0      -7.70 USD  Expenses:Bank-Fees
      7.50 USD     150.00 USD     142.50 USD  Income:Donations
--------------------------------------------
     15.20 USD    2675.67 USD    2660.47 USD

</pre>

#### tmw

<pre>
         debit         credit          total
--------------------------------------------
             0      38.57 USD      38.57 USD  Equity:Net-Assets:Restricted
      1.19 USD              0      -1.19 USD  Expenses:Bank-Fees
      1.00 USD      20.00 USD      19.00 USD  Income:Donations
--------------------------------------------
      2.19 USD      58.57 USD      56.38 USD

</pre>

#### translatewiki.net

<pre>
         debit         credit          total
--------------------------------------------
             0     121.79 USD     121.79 USD  Equity:Net-Assets:Restricted
      2.75 USD              0      -2.75 USD  Expenses:Bank-Fees
      0.75 USD      15.00 USD      14.25 USD  Income:Donations
--------------------------------------------
      3.50 USD     136.79 USD     133.29 USD

</pre>

#### tux4kids

<pre>
         debit         credit          total
--------------------------------------------
             0   16865.56 USD   16865.56 USD  Equity:Net-Assets:Restricted

</pre>

#### xorg

<pre>
         debit         credit          total
--------------------------------------------
             0  166167.83 USD  166167.83 USD  Equity:Net-Assets:Restricted
   7438.03 USD              0   -7438.03 USD  Expenses
    137.55 USD              0    -137.55 USD    Bank-Fees
   7300.48 USD              0   -7300.48 USD    IT:Hosting
   2565.00 USD   51300.03 USD   48735.03 USD  Income:Donations
--------------------------------------------
  10003.03 USD  217467.86 USD  207464.83 USD

</pre>

#### yafaray

<pre>
         debit         credit          total
--------------------------------------------
             0    5448.50 USD    5448.50 USD  Equity:Net-Assets:Restricted
      0.66 USD              0      -0.66 USD  Expenses:Bank-Fees
      0.25 USD       5.00 USD       4.75 USD  Income:Donations
--------------------------------------------
      0.91 USD    5453.50 USD    5452.59 USD
</pre>

